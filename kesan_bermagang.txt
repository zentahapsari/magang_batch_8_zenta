Halo saya Zenta,
Bermagang selama tiga minggu ini tentu memiliki kesan yang menarik untuk diceritakan.
Selama magang yaitu masa orientasi di Javan saya senang mengerjakan berbagai hal baru melalui e-learning 
dan mendapat wawasan baru yang menarik pada hari berkualitas setiap hari jumat.
Namun sering kali saya memiliki kendala sinyal setiap jam 9 pagi dan ketika memasuki waktu sore hari bahkan hingga malam hari.
Penyebab utamanya adalah memang di daerah tempat tinggal saya sinyal sering down, entah itu jaringan wifi ataupun jaringan seluler.
Oleh karena itu, ketika ada meeting online di waktu tersebut saya tidak dapat menyimak dan merespon dengan maksimal karena gangguan sinyal
yang menyebabkan meeting online yang saya ikuti putus-putus dan mohon maaf tak jarang seketika akun saya keluar dari meeting online tanpa izin.